import jwt
from jwt.exceptions import InvalidSignatureError
from flask import Blueprint, request, jsonify, current_app
from werkzeug.exceptions import Unauthorized, NotFound
from project import db
from project.users.models import User
from project.users.serializers import user_schema, login_schema


user_blueprint = Blueprint('users', __name__)


def save_user(user):
    db.session.add(user)
    db.session.commit()


@user_blueprint.route('/users', methods=['POST'])
def create():
    user = user_schema.load(request.get_json())

    save_user(user)

    return user_schema.dump(user), 201


@user_blueprint.route('/users/<id>', methods=['PUT'])
def update(id):
    user = User.query.filter_by(id=id).first()

    if user is None:
        raise NotFound

    user = user_schema.load(request.get_json(), instance=user)

    save_user(user)

    return user_schema.dump(user), 200


@user_blueprint.route('/users/<id>', methods=['PATCH'])
def patch(id):
    user = User.query.filter_by(id=id).first()
    user = user_schema.load(request.get_json(), instance=user, partial=True)

    save_user(user)

    return user_schema.dump(user), 200


@user_blueprint.route('/users', methods=['GET'])
def list():
    users = User.query.all()

    return jsonify(user_schema.dump(users, many=True)), 200


@user_blueprint.route('/login', methods=['GET', 'POST'])
def login():
    login = login_schema.load(request.get_json())
    user = User.query.filter_by(
        email=login['email'], password=login['password']).first()
    if user:
        return create_token(user)

    raise NotFound


@user_blueprint.route('/check_user', methods=['GET'])
def check_user():
    auth_header = request.headers.get('Authorization', '')
    if not auth_header.startswith('Bearer '):
        raise Unauthorized
    partes = auth_header.split(' ')
    if len(partes) != 2:
        raise Unauthorized

    token = partes[1]

    secret = current_app.config.get('SECRET')
    try:
        payload = jwt.decode(token, secret)
    except InvalidSignatureError:
        raise Unauthorized

    user = User.query.filter_by(id=payload['sub']).first()

    return user_schema.dump(user), 200


def create_token(user):
    payload = {'sub': user.id}
    secret = current_app.config.get('SECRET')
    return jwt.encode(payload, secret, algorithm='HS256').decode()
