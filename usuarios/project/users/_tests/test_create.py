from project.users.models import User
from project._tests.base import BaseTestCase


class TestCreate(BaseTestCase):
    def _call_create(self, data):
        with self.client:
            return self.client.post(
                '/users',
                json=data
            )

    def test_create(self):
        data = {
            "name": "Francisco",
            "email": "a@a.cl",
            "password": "123456"
        }

        self.assertEquals(0, User.query.count())
        response = self._call_create(data)
        self.assertStatus(response, 201)
        self.assertEquals(1, User.query.count())
        json_response = response.json
        self.assertEquals(json_response['name'], data['name'])
        self.assertEquals(json_response['email'], data['email'])
        self.assertNotIn('password', json_response)

    def test_create_without_name(self):
        data = {
            "email": "a@a.cl"
        }

        response = self._call_create(data)
        self.assertStatus(response, 400)
        json_response = response.json
        self.assertIn('name', json_response)
        self.assertEquals(
            json_response['name'][0], 'Missing data for required field.')

    def test_create_without_email(self):
        data = {
            "name": "Francisco"
        }

        response = self._call_create(data)
        self.assertStatus(response, 400)

    def test_create_without_password(self):
        data = {
            "name": "Francisco",
            "email": "a@a.cl"
        }

        response = self._call_create(data)
        self.assertStatus(response, 400)
