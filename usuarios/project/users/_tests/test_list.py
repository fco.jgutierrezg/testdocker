from project import db
from project.users.models import User, Address
from project._tests.base import BaseTestCase
from project.users._tests.utils import create_user


class TestList(BaseTestCase):
    def test_list(self):
        user1 = create_user()
        user2 = create_user()
        db.session.add(user1)
        db.session.add(user2)
        db.session.commit()

        self.assertEquals(2, User.query.count())
        with self.client:
            response = self.client.get(
                '/users'
            )
        self.assertStatus(response, 200)
        json_response = response.json
        self.assertEquals(2, len(json_response))
        self.assertEquals(user1.name, json_response[0]['name'])
        self.assertEquals(user2.name, json_response[1]['name'])

    def test_list_return_user_addresses(self):
        user = create_user()
        address = Address(street='Prueba', user=user)
        db.session.add(user)
        db.session.add(address)

        db.session.commit()

        with self.client:
            response = self.client.get(
                '/users'
            )
        self.assertStatus(response, 200)
        json_response = response.json
        self.assertEquals(1, len(json_response))
        self.assertIn('addresses', json_response[0])
        self.assertEquals(1, len(json_response[0]['addresses']))
