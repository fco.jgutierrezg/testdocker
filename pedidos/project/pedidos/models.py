from sqlalchemy.sql import func
from project import db


class Pedido(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    descripcion = db.Column(db.String, nullable=False)
    fecha_creacion = db.Column(db.DateTime, default=func.now(), nullable=False)
    usuario_id = db.Column(db.Integer, nullable=False)


class Usuario:
    def __init__(self, id, email, name):
        self.id = id
        self.email = email
        self.name = name
