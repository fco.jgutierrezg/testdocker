import requests
from functools import wraps
from flask import Blueprint, request, jsonify
from werkzeug.exceptions import Unauthorized, NotFound
from project import db
from project.pedidos.models import Pedido
from project.pedidos.serializers import pedido_schema, usuario_schema


pedidos_blueprint = Blueprint('pedidos', __name__)


def save_pedido(pedido):
    db.session.add(pedido)
    db.session.commit()


def auth():
    auth_header = request.headers.get('Authorization')
    response = requests.get(
        'http://usuarios:5000/check_user',
        headers={'Authorization': auth_header})

    if response.status_code == 401:
        raise Unauthorized

    return usuario_schema.load(response.json())


def autenticar(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        usuario = auth()
        return f(usuario, *args, **kwargs)
    return wrapper


@pedidos_blueprint.route('/pedidos', methods=['POST'])
@autenticar
def create(usuario):
    data = request.get_json()
    data['usuario_id'] = usuario.id

    pedido = pedido_schema.load(data)

    save_pedido(pedido)

    return pedido_schema.dump(pedido), 201


@pedidos_blueprint.route('/pedidos', methods=['GET'])
@autenticar
def list(usuario):
    pedidos = Pedido.query.filter_by(usuario_id=usuario.id).all()

    return jsonify(pedido_schema.dump(pedidos, many=True)), 200


@pedidos_blueprint.route('/pedidos/<id>', methods=['GET'])
@autenticar
def get(usuario, id):
    pedido = Pedido.query.filter_by(id=id, usuario_id=usuario.id).first()

    if pedido is None:
        raise NotFound

    return jsonify(pedido_schema.dump(pedido)), 200
